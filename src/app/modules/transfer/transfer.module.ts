import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TransferComponent} from '@modules/transfer/pages/transfer/transfer.component';
import {TransferRoutingModule} from '@modules/transfer/transfer-routing.module';
import { TransferFormComponent } from './shared/component/transfer-form/transfer-form.component';
import { TransferListComponent } from './shared/component/transfer-list/transfer-list.component';
import {SharedModule} from '@shared/shared.module';
import {ModalComponent} from '@shared/components/modal/modal.component';

@NgModule({
  declarations: [TransferComponent, TransferFormComponent, TransferListComponent, ModalComponent],
  imports: [
    CommonModule,
    TransferRoutingModule,
    SharedModule
  ]
})
export class TransferModule { }
