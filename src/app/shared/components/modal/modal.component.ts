// TODO refactor : use angular cdk create modal

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() show = false;
  @Input() headerText: string;
  constructor() { }

  ngOnInit(): void {
  }

}
