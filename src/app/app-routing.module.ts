import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LayoutComponent} from '@layouts/layout/layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'transfer',
    pathMatch: 'full'
  },
  {
    path: 'transfer',
    component: LayoutComponent,
    loadChildren: () =>
      import('@modules/transfer/transfer.module').then(m => m.TransferModule)
  },
  { path: '**', redirectTo: 'transfer', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
