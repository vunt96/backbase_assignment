import {HttpClient, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, BehaviorSubject} from 'rxjs';
import {data} from '../../../../mock/transactions.json';

@Injectable({
  providedIn: 'root'
})
export class TransferService {
  transfer$ = new BehaviorSubject(data);

  constructor(private readonly http: HttpClient) {
  }

}
