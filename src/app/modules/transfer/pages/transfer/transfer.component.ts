import { Component, OnInit } from '@angular/core';
import { data } from '../../../../../mock/transactions.json';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {
  rawData;
  constructor() { }

  ngOnInit(): void {
    this.rawData = data;
  }

}
