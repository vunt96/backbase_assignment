import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TransferComponent} from '@modules/transfer/pages/transfer/transfer.component';


export const routes: Routes = [
  {
    path: '',
    component: TransferComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TransferRoutingModule { }
