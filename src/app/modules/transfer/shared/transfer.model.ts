export interface ITransaction {
  categoryCode: string;
  dates: {
    valueDate?: number | string;
  };
  merchant: {
    accountNumber: string;
    name: string;
  };
  transaction: {
    amountCurrency: {
      amount: string | number;
      currencyCode: string;
    },
    creditDebitIndicator?: string;
    type: string;
  };
  allTextDisplay?: string;
  url?: string;
}
