import {Component, Input, OnInit} from '@angular/core';
import {ITransaction} from '@modules/transfer/shared/transfer.model';
import {TransferService} from '@modules/transfer/shared/transfer.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {SORT_STATE} from '@modules/transfer/shared/transfer.cost';

@Component({
  selector: 'app-transfer-list',
  templateUrl: './transfer-list.component.html',
  styleUrls: ['./transfer-list.component.scss']
})
export class TransferListComponent implements OnInit {
  filterKey: string;
  search$ = new Subject<string>();
  sortState;
  SORT_STATE = SORT_STATE;
  listTransactions: ITransaction[];
  listTransactionsRaw: ITransaction[];

  constructor(private transferService: TransferService) {
  }

  ngOnInit(): void {
    this.listTransactions = this.transferService.transfer$.value;
    this.initData();
  }

  initData() {
    this.transferService.transfer$.subscribe(data => {
      this.listTransactions = data;
      this.listTransactionsRaw = data;
    });
    this.search$.pipe(
      debounceTime(500),
      distinctUntilChanged()).subscribe((key) => {
        this.filterData();
      }
    );
  }

  filterData() {
    console.log(this.filterKey);
    if (this.filterKey) {
      this.listTransactions = this.listTransactionsRaw.filter(item =>
        item.merchant.name.toUpperCase().includes(this.filterKey.toUpperCase())
        || item.transaction.type.toUpperCase().includes(this.filterKey.toUpperCase())
      );
    } else {
      this.listTransactions = this.listTransactionsRaw;
    }
  }

  clearSearch() {
    this.search$.next('');
    this.filterKey = null;
  }

  sortList(type) {
    this.sortState = type;
    const typeValue = type.includes('ASC') ? 1 : -1;
    console.log(this.sortState, typeValue);
    if (SORT_STATE.DATE_ASC === type || SORT_STATE.DATE_DESC === type) {
      this.listTransactions.sort((a, b) => {
        const d1 = (new Date(a.dates.valueDate)).getTime();
        const d2 = (new Date(b.dates.valueDate)).getTime();
        return d1 > d2 ? typeValue : d1 < d2 ? typeValue * -1 : 0;
      });
    } else if (SORT_STATE.AMOUNT_ASC === type || SORT_STATE.AMOUNT_DESC === type) {
      this.listTransactions.sort(
        (a, b) => {
          return a.transaction.amountCurrency.amount > b.transaction.amountCurrency.amount ? typeValue : typeValue * -1;
        }
      );
    } else if (SORT_STATE.BENEFICIARY_DESC === type || SORT_STATE.BENEFICIARY_ASC === type) {
      this.listTransactions.sort(
        (a, b) => {
          return a.merchant.name > b.merchant.name ? typeValue : typeValue * -1;
        }
      );
    }
  }
}
