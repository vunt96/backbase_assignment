import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {regexCheckDecimal} from '@modules/transfer/shared/transfer.cost';
import {TransferService} from '@modules/transfer/shared/transfer.service';

@Component({
  selector: 'app-transfer-form',
  templateUrl: './transfer-form.component.html',
  styleUrls: ['./transfer-form.component.scss']
})
export class TransferFormComponent implements OnInit {
  formTransfer: FormGroup;
  isInput = true;
  totalAmount = 5824.76;
  showInValidAmount = false;
  submitSuccess = false;
  titlePopup = 'Transaction Info';

  constructor(private fb: FormBuilder, private transferService: TransferService) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  protected initForm = () => {
    this.formTransfer = this.fb.group(this.controlsConfig());
  }

  controlsConfig() {
    return {
      fromAccount: new FormControl({value: this.getValueFromAccount(), disabled: true}),
      toAccount: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      amount: new FormControl(null, [
        Validators.required,
        Validators.pattern(regexCheckDecimal)
      ])
    };
  }

  reviewTransaction() {
    console.log(this.formTransfer);
    const amountNew = this.totalAmount - this.formTransfer.value.amount;
    if (amountNew > -500) {
      this.isInput = false;
    } else {
      this.showInValidAmount = true;
    }
  }

  cancelTransaction() {
    this.isInput = true;
  }

  confirmTransaction() {
    const {fromAccount, toAccount, amount} = this.formTransfer.value;
    const amountNew = this.totalAmount - amount;
    const dataNew = {
      categoryCode: '#12a580',
      dates: {
        valueDate: new Date().getTime()
      },
      transaction: {
        amountCurrency: {
          amount,
          currencyCode: 'USD'
        },
        type: 'Salaries',
        creditDebitIndicator: 'CRDT'
      },
      merchant: {
        name: toAccount,
        accountNumber: 'SI64397745065188826'
      },
      url: 'assets/icons/backbase.png'
    };
    const rawData = this.transferService.transfer$.value;
    this.totalAmount = this.totalAmount - amount;
    rawData.unshift(dataNew);
    this.transferService.transfer$.next(rawData);
    this.submitSuccess = true;
  }

  getValueFromAccount() {
    return '[VUNT13] Nguyen Tuan Vu - $' + this.roundToTwo(this.totalAmount);
  }

  roundToTwo(num) {
    const a = +(Math.round(Number(num + 'e+2')) + 'e-2');
    return isNaN(a) ? 0 : a;
  }

  closeModal() {
    this.submitSuccess = false;
    this.formTransfer.reset();
    this.initForm();
    this.isInput = true;
    this.titlePopup = '';
  }
}
